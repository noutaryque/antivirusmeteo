#!/bin/bash
#!/usr/bin/env gnuplot


#  $$$$$$$\                   $$$$$$\            $$\                               
#  $$  __$$\                 $$  __$$\           \__|                              
#  $$ |  $$ |$$\   $$\       $$ /  $$ |$$\   $$\ $$\ $$$$$$$\   $$$$$$\  $$\   $$\ 
#  $$$$$$$\ |$$ |  $$ |      $$ |  $$ |$$ |  $$ |$$ |$$  __$$\ $$  __$$\ $$ |  $$ |
#  $$  __$$\ $$ |  $$ |      $$ |  $$ |$$ |  $$ |$$ |$$ |  $$ |$$ /  $$ |$$ |  $$ |
#  $$ |  $$ |$$ |  $$ |      $$ $$\$$ |$$ |  $$ |$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |
#  $$$$$$$  |\$$$$$$$ |      \$$$$$$ / \$$$$$$  |$$ |$$ |  $$ |\$$$$$$  |\$$$$$$  |
#  \_______/  \____$$ |       \___$$$\  \______/ \__|\__|  \__| \______/  \______/ 
#            $$\   $$ |           \___|                                            
#            \$$$$$$  |                                                            
#             \______/                                                             


# Enregistrer le temps avant l'exécution du script
debut=$(date +%s.%N)

# Nom de l'exécutable à vérifier
NOM_EXECUTABLE="exe"

# Vérifie si l'exécutable est présent
if [ ! -x "$NOM_EXECUTABLE" ]; then
    echo "L'exécutable $NOM_EXECUTABLE n'est pas présent. Lancement de la compilation..."
    # Commande de compilation
    make
    if [ $? -eq 0 ]; then
        echo "La compilation s'est terminée avec succès."
    else
        echo "La compilation a échoué. Abandon."
        exit 1
    fi
fi

# Initialisation des options
declare -A options=(
    ["F"]=0
    ["G"]=0
    ["S"]=0
    ["A"]=0
    ["O"]=0
    ["Q"]=0
    ["D"]=0
    ["t"]=0
    ["m"]=0
    ["w"]=0
    ["h"]=0
    ["p"]=0
)
option_aide=0
MIN=0
MAX=0

# Boucle sur les arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        -F) options["F"]=1;;
        -G) options["G"]=1;;
        -S) options["S"]=1;;
        -A) options["A"]=1;;
        -O) options["O"]=1;;
        -Q) options["Q"]=1;;
        -d) options["D"]=1; MIN="$2"; MAX="$3"; shift; shift        ;;
        --help)
        option_aide=1
        echo "Utilisation : mon_script.sh [-F|-G|-S|-A|-O|-Q] [-d min max]";;
        -t) options["t"]=-t
            tmode="$2";;
        -w) options["w"]=-w;;
        -p) options["p"]=-p
            pmode="$2";;
        -m) options["m"]=-m;;
        -h) options["h"]=-h;;
        -i) entree="$2";;
        -o) sortie="$2";;
    esac
    shift
done


barre_erreur() {
    entree="$2"
    sortie="$3"
    gnuplot <<- EOF
    set title "Diagramme $1 Barre Erreur"
    set style data histograms
    set datafile separator ";"
    set style histogram errorbars
    set ylabel "$4 Moyenne"
    set xlabel "ID Station"
    set terminal png
    set output "${sortie}.png"
    plot "${entree}" using 1:2:3:4 with yerrorbars nokey
EOF
}

ligne() {
    entree="$2"
    sortie="$3"
    gnuplot -percist <<- EOF
    set title "Diagramme en Ligne $1"
    set grid
    set style data lines
    set datafile separator ";"
    set ylabel "$4 Moyenne"
    set xlabel "Jours/Heures"
    set xdata time
    set timefmt "%d\T%H"
    set format x "%d/%H"
    set xtics rotate
    set terminal png
    set output "${sortie}.png"
    plot "${entree}" using 1:2 with lines notitle nokey
EOF
}

vecteur() {
    entree="$2"
    sortie="$3"
    gnuplot -persist <<- EOF
    set title "Diagramme Vecteur $1"
    set grid
    set datafile separator ";"
    set xlabel "Longitude (Ouest-Est)"
    set ylabel "Latitude (Nord-Sud)"
    set key at screen 0.8,0.8
    set terminal png
    set output "${sortie}.png"
    set nokey
    plot "${entree}" using 2:1:4:3 with vectors head filled lc rgb "red"
EOF
}

carte() {
    entree="$2"
    sortie="$3"
    gnuplot -persist <<- EOF
    set title "Carte interpolée et colorée des Températures $1"
    set xyplan relative 0
    set style data pm3d
    set style function pm3d
    set datafile separator ";"
    plot "${entree}" using 2:1:3 with pm3d
EOF
}



# Fonction pour vérifier si plusieurs options ont été activées
verif_options_multiples() {
    local options_active=0
    for value in "${options[@]}"; do
        if [[ $value -eq 1 ]]; then
            (( options_active++ ))
        fi
    done
    if [[ $options_active -gt 1 ]]; then
        echo "Plusieurs options ont été activées. Veuillez n'activer qu'une seule option."
        exit 1
    fi
}

verif_options_multiples

function verif_success() {
  if [ $? -eq 0 ]; then
    echo "La commande s'est terminée avec succès."
  else
    echo "La commande a échoué. Abandon."
    exit 1
  fi
}



# Vérifie si aucune option n'est activée
if [[ ${options["F"]} -eq 0 && ${options["G"]} -eq 0 && ${options["S"]} -eq 0 && ${options["A"]} -eq 0 && ${options["O"]} -eq 0 && ${options["Q"]} -eq 0 && ${options["D"]} -eq 0 && option_aide -eq 0 ]]; then
    echo "Aucune option n'a été activée. + aucune limite sur la position des relevés"
    if [[ modet -eq 1 ]]; then
      barre_erreur "Aucun Pays" "$sortie" mode_temp_1 Température
    fi

    if [[ modep -eq 1 ]]; then
      barre_erreur "Aucun Pays" "$sortie" mode_press_1 Pression
    fi

    if [[ modet -eq 3 ]]; then
      barre_erreur "Aucun Pays" "$sortie" mode_temp_3 Temperature
    fi

    if [[ modep -eq 3 ]]; then
      ligne "Aucun Pays" "$sortie" mode_press_3 Pression
    fi

    if [[ ${options["w"]}=-w ]]; then
      vecteur "Aucun Pays" "$sortie" Vent
    fi

    if [[ ${options["h"]}=-h ]]; then
      carte "Aucun Pays" "$sortie" carte_hauteur Hauteur
    fi

    if [[ ${options["m"]}=-m ]]; then
      carte "Aucun Pays" "$sortie" carte_humidité Humidité
    fi
else
    if [[ ${options["F"]} -eq 1 ]]; then

      #IMPLEMENTER LE ./exe
      if [[ modet -eq 1 ]]; then
        barre_erreur "France" "$sortie" mode_temp_1 Température
      fi

      if [[ modep -eq 1 ]]; then
        barre_erreur "France" "$sortie" mode_press_1 Pression
      fi

      if [[ modet -eq 3 ]]; then
        barre_erreur "France" "$sortie" mode_temp_3 Temperature
      fi

      if [[ modep -eq 3 ]]; then
        ligne "France" "$sortie" mode_press_3 Pression
      fi

      if [[ ${options["w"]}=-w ]]; then
        vecteur "France" "$sortie" Vent
      fi

      if [[ ${options["h"]}=-h ]]; then
        carte "France" "$sortie" carte_hauteur Hauteur
      fi

      if [[ ${options["m"]}=-m ]]; then
        carte "France" "$sortie" carte_humidité Humidité
      fi
    elif [[ ${options["G"]} -eq 1 ]]; then

      #IMPLEMENTER LE ./exe
      if [[ modet -eq 1 ]]; then
        barre_erreur "Guyane" "$sortie" mode_temp_1 Température
      fi

      if [[ modep -eq 1 ]]; then
        barre_erreur "Guyane" "$sortie" mode_press_1 Pression
      fi

      if [[ modet -eq 3 ]]; then
        barre_erreur "Guyane" "$sortie" mode_temp_3 Temperature
      fi

      if [[ modep -eq 3 ]]; then
        ligne "Guyane" "$sortie" mode_press_3 Pression
      fi

      if [[ ${options["w"]}=-w ]]; then
        vecteur "Guyane" "$sortie" Vent
      fi

      if [[ ${options["h"]}=-h ]]; then
        carte "Guyane" "$sortie" carte_hauteur Hauteur
      fi

      if [[ ${options["m"]}=-m ]]; then
        carte "Guyane" "$sortie" carte_humidité Humidité
      fi
    elif [[ ${options["S"]} -eq 1 ]]; then

      #IMPLEMENTER LE ./exe
      if [[ modet -eq 1 ]]; then
        barre_erreur "Guyane" "$sortie" mode_temp_1 Température
      fi

      if [[ modep -eq 1 ]]; then
        barre_erreur "Guyane" "$sortie" mode_press_1 Pression
      fi

      if [[ modet -eq 3 ]]; then
        barre_erreur "Guyane" "$sortie" mode_temp_3 Temperature
      fi

      if [[ modep -eq 3 ]]; then
        ligne "Guyane" "$sortie" mode_press_3 Pression
      fi

      if [[ ${options["w"]}=-w ]]; then
        vecteur "Guyane" "$sortie" Vent
      fi

      if [[ ${options["h"]}=-h ]]; then
        carte "Guyane" "$sortie" carte_hauteur Hauteur
      fi

      if [[ ${options["m"]}=-m ]]; then
        carte "Guyane" "$sortie" carte_humidité Humidité
      fi
    elif [[ ${options["A"]} -eq 1 ]]; then

      #IMPLEMENTER LE ./exe
      if [[ modet -eq 1 ]]; then
        barre_erreur "Antilles" "$sortie" mode_temp_1 Température
      fi

      if [[ modep -eq 1 ]]; then
        barre_erreur "Antilles" "$sortie" mode_press_1 Pression
      fi

      if [[ modet -eq 3 ]]; then
        barre_erreur "Antilles" "$sortie" mode_temp_3 Temperature
      fi

      if [[ modep -eq 3 ]]; then
        ligne "Antilles" "$sortie" mode_press_3 Pression
      fi

      if [[ ${options["w"]}=-w ]]; then
        vecteur "Antilles" "$sortie" Vent
      fi

      if [[ ${options["h"]}=-h ]]; then
        carte "Antilles" "$sortie" carte_hauteur Hauteur
      fi

      if [[ ${options["m"]}=-m ]]; then
        carte "Antilles" "$sortie" carte_humidité Humidité
      fi
    elif [[ ${options["O"]} -eq 1 ]]; then
      #IMPLEMENTER LE ./exe


      if [[ modet -eq 1 ]]; then
        barre_erreur "Océan Indien" "$sortie" mode_temp_1 Température
      fi

      if [[ modep -eq 1 ]]; then
        barre_erreur "Océan Indien" "$sortie" mode_press_1 Pression
      fi

      if [[ modet -eq 3 ]]; then
        barre_erreur "Océan Indien" "$sortie" mode_temp_3 Temperature
      fi

      if [[ modep -eq 3 ]]; then
        ligne "Océan Indien" "$sortie" mode_press_3 Pression
      fi

      if [[ ${options["w"]}=-w ]]; then
        vecteur "Océan Indien" "$sortie" Vent
      fi

      if [[ ${options["h"]}=-h ]]; then
        carte "Océan Indien" "$sortie" carte_hauteur Hauteur
      fi

      if [[ ${options["m"]}=-m ]]; then
        carte "Océan Indien" "$sortie" carte_humidité Humidité
      fi
    elif [[ ${options["Q"]} -eq 1 ]]; then
      if [[ modet -eq 1 ]]; then
        barre_erreur "Antarctique" "$sortie" mode_temp_1 Température
      fi

      if [[ modep -eq 1 ]]; then
        barre_erreur "Antarctique" "$sortie" mode_press_1 Pression
      fi

      if [[ modet -eq 3 ]]; then
        barre_erreur "Antarctique" "$sortie" mode_temp_3 Temperature
      fi

      if [[ modep -eq 3 ]]; then
        ligne "Antarctique" "$sortie" mode_press_3 Pression
      fi

      if [[ ${options["w"]}=-w ]]; then
        vecteur "Antarctique" "$sortie" Vent
      fi

      if [[ ${options["h"]}=-h ]]; then
        carte "Antarctique" "$sortie" carte_hauteur Hauteur
      fi

      if [[ ${options["m"]}=-m ]]; then
        carte "Antarctique" "$sortie" carte_humidité Humidité
      fi
    elif [[ ${options["D"]} -eq 1 ]]; then


    #IMPLEMENTER LE ./exe

      if [[ modet -eq 1 ]]; then
        barre_erreur "Aucun Pays" "$sortie" mode_temp_1 Température
      fi

      if [[ modep -eq 1 ]]; then
        barre_erreur "Aucun Pays" "$sortie" mode_press_1 Pression
      fi

      if [[ modet -eq 3 ]]; then
        barre_erreur "Aucun Pays" "$sortie" mode_temp_3 Temperature
      fi

      if [[ modep -eq 3 ]]; then
        ligne "Aucun Pays" "$sortie" mode_press_3 Pression
      fi

      if [[ ${options["w"]}=-w ]]; then
        vecteur "Aucun Pays" "$sortie" Vent
      fi

      if [[ ${options["h"]}=-h ]]; then
        carte "Aucun Pays" "$sortie" carte_hauteur Hauteur
      fi

      if [[ ${options["m"]}=-m ]]; then

        carte "Aucun Pays" "$sortie" carte_humidite Humidité
      fi
    fi
fi




# Enregistrer le temps après l'exécution du script
fin=$(date +%s.%N)

# Calculer le temps d'exécution total avec une précision de 0.1s
temps=$(echo "($fin - $debut) / 1" | awk '{printf "%.1f\n", $1}')
printf "Temps d'exécution total : %.1f secondes\n" $temps
