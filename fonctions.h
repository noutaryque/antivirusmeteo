#ifndef __fonctions_H_
#define __fonctions_H_
#include <stdlib.h>
#include <stdio.h>

//Definition des structures

//Structure d'un arbre pour la date
typedef struct noeudDate{
    char* date;
    int temp;
    struct noeudDate* fils_g;
    struct noeudDate* fils_d;
}noeudDate;
typedef noeudDate* arbreDate;

//Structure temp pour contenir min, max, moy , et sa valeur
typedef struct temp{
  float min;
  float max;
  float moyenne;
  int nbval;
}temp;

//Structure pour contenir la moyenne des vitesse et des degres
typedef struct vent{
    float moyennevitesse;
    float moyennedegre;
    int nbval;
}vent;

typedef struct donnee{
    int nstation;
    int hauteur;
    int humidite;
    int longitude;
    int latitude;
    temp* temp;
    arbreDate date;
    vent* vent;
}donnee;
typedef donnee* valeurs;

typedef struct noeud{
    valeurs val;
    struct noeud* fils_g;
    struct noeud* fils_d;
}noeud;
typedef noeud* arbre;

//Prototypage des fonctions
arbre lire(char* name, int nb1, int nb2, int nb3, char* type, int filtre, char* arg1, char* arg2);
arbre insertionarbreTempPress(arbre a, int nstation, float val);
arbre insertionarbreVent(arbre a, int nstation, int degre, float vitesse);
arbre insertionarbreHauteur(arbre a, int nstation, int hauteur);
arbre maxhumiditestation(arbre a, int nstation, int humidite);
arbre insertionarbreHumidite(arbre a, int nstation, int humidite);
arbre insertionarbreDate(arbre a, int nstation,char* date, int temp);
time_t iso8601_to_unix(char *iso8601);
arbre insertionarbreHumiditerec(arbre a,arbre b);
void sauvegarderArbreRec(arbre a, FILE* fichier,char* type);

#endif
//FIN
