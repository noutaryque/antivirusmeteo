# AntivirusMeteo
membres: GROUPE : Quentin Noutary Gwenn Bourillon Nolhan Aure Paul Guignard

L'objectif de ce projet est de traiter un grand nombre de données le plus rapidement possible(avec la meilleure complexité) et de faire un graphique de ces traitements, nous avons réaliser les fonctions suivantes:

-mode 1 : L'objectif est de trier le fichier par station et de calculer les temperature minimum maximum et moyenne : on appelle la fonction comme ceci : ./exe -i nomFichier.csv -o nomDuFichierSorti -t 1

-mode 2 : L'objectif est de trier le fichier par date et de calculer la moyenne pour une certaine date, cette fonction ne marche pas dans notre projet, nous avons des erreures on appelle la fonction comme ceci : ./exe -i nomFichier.csv -o nomDuFichierSorti -t 3

-mode 3 : L'objectif est de trier le fichier par date et de trier par numero de station quand la date est la meme, cette fonction est trop lente elle met trop de temps pour le fichier, on appelle la fonction comme ceci : ./exe -i nomFichier.csv -o nomDuFichierSorti -t 3

-vent : L'objectif est de trier le fichier par station et de calculer la moyenne des degre et de la vitesse du vent, on appelle la fonction comme ceci : ./exe -i nomFichier.csv -o nomDuFichierSorti -w

-hauteur : L'objectif est de trier le fichier par hauteur, on appelle la fonction comme ceci : ./exe -i nomFichier.csv -o nomDuFichierSorti -h

-humidité : L'objectif est de calculer le maximum des humidités de chaque station et de trier le fichier par humidité, on appelle la fonction comme ceci : ./exe -i nomFichier.csv -o nomDuFichierSorti -m

-longitude l'objectif est de filtrer les données voulues en fonction de l'intervale donné par l'utilisateur on appelle la fonction en ajoutant : -g longitudemin longitudemax

-latitude l'objectif est de filtrer les données voulues en fonction de l'intervale donné par l'utilisateur on appelle la fonction en ajoutant : -a latitudemin latitudemax

-date l'objectif est de filtrer les données voulues en fonction de l'intervale donné par l'utilisateur on appelle la fonction en ajoutant : -a datemin datemax

# Status du Projet
                                                      en developement
❗❗❗❗IMPORTANT❗❗❗❗
 --SCRIPT SHELL EN DEVELOPEMENT, NE FONCTIONNE PAS POUR LE MOMENT
 --ERREUR AVEC LES DATES (mode 2,3) et -d<min><max> (L'erreur viens du free dans la fonction lire)


