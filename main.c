#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <stdbool.h>
#include <time.h>
#include "fonctions.h"


/***
 *                           __      __             __                                                                __
 *                          /  |    /  |           /  |                                                              /  |
 *      ______   _______   _$$ |_   $$/  __     __ $$/   ______   __    __   _______        _____  ____    ______   _$$ |_     ______    ______
 *     /      \ /       \ / $$   |  /  |/  \   /  |/  | /      \ /  |  /  | /       |      /     \/    \  /      \ / $$   |   /      \  /      \
 *     $$$$$$  |$$$$$$$  |$$$$$$/   $$ |$$  \ /$$/ $$ |/$$$$$$  |$$ |  $$ |/$$$$$$$/       $$$$$$ $$$$  |/$$$$$$  |$$$$$$/   /$$$$$$  |/$$$$$$  |
 *     /    $$ |$$ |  $$ |  $$ | __ $$ | $$  /$$/  $$ |$$ |  $$/ $$ |  $$ |$$      \       $$ | $$ | $$ |$$    $$ |  $$ | __ $$    $$ |$$ |  $$ |
 *    /$$$$$$$ |$$ |  $$ |  $$ |/  |$$ |  $$ $$/   $$ |$$ |      $$ \__$$ | $$$$$$  |      $$ | $$ | $$ |$$$$$$$$/   $$ |/  |$$$$$$$$/ $$ \__$$ |
 *    $$    $$ |$$ |  $$ |  $$  $$/ $$ |   $$$/    $$ |$$ |      $$    $$/ /     $$/       $$ | $$ | $$ |$$       |  $$  $$/ $$       |$$    $$/
 *     $$$$$$$/ $$/   $$/    $$$$/  $$/     $/     $$/ $$/        $$$$$$/  $$$$$$$/        $$/  $$/  $$/  $$$$$$$/    $$$$/   $$$$$$$/  $$$$$$/
 *
 *
 * By Quinou
 */


void menu_aide(){
  printf("----------OPTIONS SUPLEMENTAIRES----------\n");
  printf("\nOptions:\n");
  printf("  --help   Affiche cette aide\n");
  printf("  -t <mode>  Voir la liste des modes\n");
  printf("  -p <mode>  Voir la liste des modes\n\n");
  printf("---------------MODES---------------\n");
  printf("    1-produit en sortie les températures (ou pressions) minimales, maximales et moyennes par station dans l’ordrecroissant du numéro de station.\n");
  printf("    2-produit en sortie les températures (ou pressions) moyennes par date/heure, triées dans l’ordrechronologique. La moyenne se fait sur toutes les stations.\n");
  printf("    3-produit en sortie les températures (ou pressions) pardate/heure par station. Elles seront triées d’abord par ordrechronologique, puis par ordre croissant de l’identifiant de la station.\n");
  printf("-----------------------------------\n\n");
  printf("  -w : vent ( (w)ind ).Produit en sortie l’orientation moyenne et la vitesse moyenne desvents pour chaque station. Quand on parle de moyenne, il s’agirade faire la somme pour chaque composante du vecteur, et d’enfaire la moyenne une fois tous les vecteurs traités. On aura doncune moyenne sur l’axe X et une moyenne sur l’axe Y : les 2 résultatsfournissant   le   module   et   l’orientation   moyens   demandés.   Lesdonnées seront triées par identifiant croissant de la station.\n");
  printf("  -h : (h)auteur.Produit en sortie la hauteur pour chaque station. Les hauteursseront triées par ordre décroissant.\n");
  printf("  -m : humidité ( (m)oisture ).Produit en sortie l’humidité maximale pour chaque station. Lesvaleurs d’humidités seront triées par ordre décroissant.\n");
  printf("  -g <min> <max> : lon(g)itude.Permet de filtrer les données de sortie en ne gardant que lesdonnées qui sont dans l’intervalle de longitudes [<min>..<max>]incluses. Le format des longitudes est un nombre réel.\n");
  printf("  -a <min> <max> :l(a)titude.Permet de filtrer les données de sortie en ne gardant que lesrelevés   qui   sont   dans   l’intervalle   de   latitudes   [<min>..<max>]incluses. Le format des latitudes est un nombre réel.\n");
  printf("  -d <min> <max> : (d)ates.Permet de filtrer les données de sortie en ne gardant que lesrelevés qui sont dans l’intervalle de dates [<min>..<max>] incluses.Le format des dates est une chaine de type YYYY-MM-DD (année-mois-jour).\n");
  printf("  -i <nom_fichier> : (f)ichier d’entrée.Permet de spécifier le chemin du  fichier CSV d’entrée (fichierfourni). Cette option est obligatoire.\n");
  printf("  -o <nom_fichier> : (f)ichier de sortie.Permet   de   donner   un   nom   au   fichier   de   sortie   contenant   lesdonnées. Si les options utilisées nécessitent plusieurs fichiers, cetargument peut servir à définir le préfixe des fichiers de sortie.Si ce champ n’est pas fourni, la sortie s’appellera ‘meteoxxxx.dat’par défaut, avec xxxx un nombre sur 4 digits.\n");
}

int multiple_opt(int nbopt){
    if (nbopt > 1) {
      printf("ERREUR, TU UTILISES DES OPTIONS NON COMBINABLE");
      return EXIT_FAILURE;
    }
    return 0;
}

int main(int argc, char **argv) {
    int option;
    arbre a;
    a = NULL;
    int succes;
    succes = 1;
    FILE* fichier;
    char* nomfichier = NULL;
    char* arg_i = NULL, *arg_tempp = NULL, *arg_o = NULL, *arg1 = NULL, *arg2 = NULL;
    arg1 = malloc(sizeof(char));
    arg2 = malloc(sizeof(char));
    char* type = NULL;
    char nom_alea[15];
    int filtre = 0;
    int nbalea = 0;

    bool opt_t_arg = true;
    bool opt_p_arg = true;
    bool opt_w_arg = false;
    bool opt_i_arg = true;
    bool opt_o_arg = true;
    bool opt_h_arg = false;
    bool opt_m_arg = false;


    int nbopt = 0;
    int opt_t; int opt_p; int opt_w; int opt_h; int opt_m;int opt_g;int opt_a;int opt_d;int opt_i;int opt_o;
    opt_t = 0; opt_p = 0; opt_w = 0; opt_h = 0; opt_m = 0; opt_g = 0; opt_a = 0; opt_d = 0; opt_i = 0; opt_o = 0;


    static struct option options_longue[] = {
        {"help",   no_argument, 0, '?'},
        {0, 0, 0, 0}
    };


    while ((option = getopt_long(argc, argv, "t:p:whmg:a:d:i:o:?", options_longue, NULL)) != -1) {


        switch (option) {
            case 't':
                if (opt_t_arg && optarg == NULL) {
                  printf("Erreur : l'option -t requiert un argument\n");
                  return 1;
                }
                nbopt++;
                arg_tempp = optarg;
                opt_t = 1;
                break;
            case 'p':
                if (opt_p_arg && optarg == NULL) {
                    printf("Erreur : l'option -p requiert un argument\n");
                    return 1;
                }
                nbopt++;
                arg_tempp = optarg;
                opt_p = 1;
                break;
            case 'w':
                if (opt_w_arg && optarg != NULL) {
                    printf("Erreur : l'option -w ne requiert pas d'argument\n");
                    return 1;
                }
                opt_w = 1;
                nbopt++;
                break;
            case 'h':
                if (opt_h_arg && optarg != NULL) {
                    printf("Erreur : l'option -h ne requiert pas d'argument\n");
                    return 1;
                }
                opt_h = 1;
                nbopt++;
                break;
            case 'm':
                if (opt_m_arg && optarg != NULL) {
                    printf("Erreur : l'option -w ne requiert pas d'argument\n");
                    return 1;
                }
                opt_m = 1;
                nbopt++;
                break;
            case 'g':
                if (optarg == NULL) { // Vérifie si un argument est fourni avec l'option -g
                    printf("Erreur : l'option -g requiert deux arguments\n");
                    return 1;
                }
                arg1 = optarg; // Récupère le premier argument
                if (optind < argc && argv[optind][0] != '-') {
                    arg2 = argv[optind++]; // Récupère le deuxième argument
                } else {
                    printf("Erreur : l'option -g requiert deux arguments\n");
                    return 1;
                }
                opt_g = 1;
                break;
            case 'a':
                if (optarg == NULL) { // Vérifie si un argument est fourni avec l'option -g
                    printf("Erreur : l'option -a requiert deux arguments\n");
                    return 1;
                }
                arg1 = optarg; // Récupère le premier argument
                if (optind < argc && argv[optind][0] != '-') {
                    arg2 = argv[optind++]; // Récupère le deuxième argument
                } else {
                    printf("Erreur : l'option -a requiert deux arguments\n");
                    return 1;
                }
                opt_a = 1;
                break;
            case 'd':
                if (optarg == NULL) { // Vérifie si un argument est fourni avec l'option -g
                    printf("Erreur : l'option -d requiert deux arguments\n");
                    return 1;
                }
                arg1 = optarg; // Récupère le premier argument
                if (optind < argc && argv[optind][0] != '-') {
                    arg2 = argv[optind++]; // Récupère le deuxième argument
                } else {
                    printf("Erreur : l'option -d requiert deux arguments\n");
                    return 1;
                }
                opt_d = 1;
                break;
            case 'i':
                if (opt_i_arg && optarg == NULL) {
                    printf("Erreur : l'option -i requiert un argument\n");
                    return 1;
                }
                arg_i = optarg;
                opt_i = 1;
                break;
            case 'o':
                if (opt_o_arg && optarg == NULL) {
                    printf("Erreur : l'option -i requiert un argument\n");
                    return 1;
                }
                arg_o = optarg;
                opt_o = 1;
                break;
            case '?':
                menu_aide();
                return 0;
                break;
            default:
                printf("Option Inconnue %s\n", argv[optind-1]);
                return 1;
                break;
        }
    }




    if (nbopt >1) {
      printf("ERREUR TU COMBINES DES OPTIONS INCOMBINABLES !!\n");
      return EXIT_FAILURE;
    }

    if (opt_i && arg_i !=NULL) {
          nomfichier = arg_i;
    }else{
          printf("OPTION -i <ARGS> OBLIGATOIRE\n\n");
          menu_aide();
          return EXIT_FAILURE;
    }

    if (!opt_o && !opt_t && !opt_p && !opt_w && !opt_h && !opt_m && !opt_g && !opt_a && !opt_d && !opt_i){
      printf("ERREUR IL FAUT AU MOINS RENSEIGNER UNE OPTION VALIDE\n");
      return EXIT_FAILURE;
    }


    if (opt_t) {

      if (arg_tempp != NULL) {

        if (atoi(arg_tempp) == 1){

          type = "tmp1";

          a = lire(nomfichier,0,10,-1,type, filtre, arg1, arg2);
        }else
        if (atoi(arg_tempp) == 2) {
          type = "tmp2";

        }else
        if (atoi(arg_tempp) == 3) {
          type = "tmp3";
          a = lire(nomfichier,0,1,10,type, filtre, arg1, arg2);

        }


      }else{
        printf("ERREUR IL FAUT AU MOINS RENSEIGNER UN MODE VALIDE\n");
      }
    }

    if (opt_p) {
      if (arg_tempp != NULL) {
        if (atoi(arg_tempp) == 1){
          type = "press1";
          a = lire(nomfichier,0,6,-1,type, filtre, arg1, arg2);
        }else
        if (atoi(arg_tempp) == 2) {
          type = "press2";
        }else
        if (atoi(arg_tempp) == 3) {
          type = "press3";
        }


      }else{
        printf("ERREUR IL FAUT AU MOINS RENSEIGNER UN MODE VALIDE\n");
      }
    }

    if (opt_w) {
      type = "vent";
      a = lire(nomfichier,0,3,4,type, filtre, arg1, arg2);
    }
    if (opt_h) {
      type = "hauteur";
      a = lire(nomfichier,0,13,-1,type, filtre, arg1, arg2);
    }
    if (opt_m) {
      type = "humidité";
      a = lire(nomfichier,0,5,-1,type, filtre, arg1, arg2);
    }
    if (opt_g) {
      filtre = 1;
      a = lire(nomfichier,-1,9,-1,type, filtre, arg1, arg2);
    }
    if (opt_a) {
      filtre = 2;
      a = lire(nomfichier,-1,9,-1,type, filtre, arg1, arg2);
    }
    if (opt_d) {
      filtre = 3;
      a = lire(nomfichier,-1,1,-1,type, filtre, arg1, arg2);
    }

    if (opt_o && arg_o !=NULL) {

      fichier = fopen(arg_o, "w");
      sauvegarderArbreRec(a,fichier, type);
      fclose(fichier);
      free(a);
    }

    if (!opt_o) {
      srand(time(NULL));
      nbalea = rand() % (9999 - 1000 + 1) + 1000;
      sprintf(nom_alea, "meteo%04d.dat", nbalea);
      fichier = fopen(nom_alea, "w");
      sauvegarderArbreRec(a,fichier, type);
      fclose(fichier);
      free(a);
    }






    return succes;
}
//FIN
