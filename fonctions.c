#include <stdlib.h>
#include <stdio.h>
#include "fonctions.h"
#include <string.h>
#include <time.h>
#define N 1000

/* auteur : Quentin
  but: recupères les coordonnées du fichier CSV et extrait la latitude et la longitude */
  void recup_latitude_longitude(char *string, float* latitude, float* longitude) {
    char* separ = strtok(string, ",");
    *latitude = atof(separ);
    separ = strtok(NULL, ",");
    *longitude = atof(separ);
}

/* auteur : Gwenn
   but : prend en parametre la longitude et l'intervalle entré par l'utilisateur et renvoie 1 si la longitude est dans le bon intervalle et 0 sinon*/
   int filtreg(float longitude,int min,int max){

  if (longitude>=min && longitude<=max){
    printf("%d %d\n", min, max );
    return 1;
  }
  return 0;
}

/* auteur : Gwenn
   but : prend en parametre la latitude et l'intervalle entré par l'utilisateur et renvoie 1 si la latitude est dans le bon intervalle et 0 sinon*/
   int filtrea(float latitude, int min, int max){
  if (latitude>=min && latitude<=max){
    return 1;
  }
  return 0;
}

/* auteur : Nolhan
   but : prend en parametre la date et l'intervalle entré par l'utilisateur et renvoie 1 si la date est dans le bon intervalle et 0 sinon*/
   int filtred(char* date, char* dmin,char* dmax){
  if ((iso8601_to_unix(date)>=iso8601_to_unix(dmin))&&(iso8601_to_unix(date)>=iso8601_to_unix(dmax))){
    return 1;
  }
  return 0;
}

/* auteur : Nolhan
   but : Transforme les dates du fichier Csv (format iso 8601) en timestamp unix */
   time_t iso8601_to_unix(char* iso8601) {
    struct tm tm;
    strptime(iso8601, "%Y-%m-%dT%H:%M:%S%z", &tm);
    return mktime(&tm);
}

/* auteur : Quentin&Gwenn
   but : Aller chercher les données dans le fichier CSV*/
  arbre lire(char* nom, int nb1, int nb2, int nb3, char* type, int filtre, char* min, char* max){

    char ligne[N];
    FILE *fichier;
    char *separ, *tofree, *str;
    int colonne = 0;
    arbre a;
    a = NULL;
    char* cl1;
    char* cl2;
    char* cl3;
    char* date;
    date = malloc(sizeof(char));
    float* latitude = NULL, *longitude = NULL;
    latitude = malloc(sizeof(float));
    longitude = malloc(sizeof(float));
    cl1 = malloc(sizeof(char));
    cl2 = malloc(sizeof(char));
    cl3 = malloc(sizeof(char));
    cl3 = NULL;
    cl1= NULL;
    cl2= NULL;

    fichier = fopen(nom, "r");
    if (fichier==NULL){
        printf("Fichier Introuvable\n");
        return NULL;
    }

    fgets(ligne, N, fichier ); //1ere ligne
    while (!feof(fichier)){
        if (fgets(ligne, N, fichier)) { // lire une ligne seulement si fgets renvoie une valeur différent de NULL
						tofree = str = strdup(ligne);

            while ((separ = strsep(&str, ";"))){
                if (strcmp(separ, "")) {
                    if (colonne == nb1) {
                        cl1 = separ;

                    }
                    if (colonne == nb2) {
                        cl2 = separ;

                    }
                    if (colonne == nb3) {
                        cl3 = separ;
                    }

                    if (filtre == 1 ||filtre == 2) {
                      if (colonne == 9) {
                        recup_latitude_longitude(separ, latitude,longitude);
                      }
                    }


                    if (filtre == 3) {
                      if (colonne==2) {
                        date = separ;
                      }
                    }

                }else{ // si la valeur est vide
						        if (colonne == nb1) {
						            cl1 = ""; // donner une valeur vide à la variable
						        }
						        if (colonne == nb2) {
						            cl2 = ""; // donner une valeur vide à la variable
						        }
						        if (colonne == nb3) {
            						cl3 = "";
										}


									}

                colonne = colonne +1;
            }
            if(!strcmp(type,"tmp1")||!strcmp(type,"press1")){// si le type utiliser correspond a tmpp (température-pressuin) et que le mode est 1
              if (filtre == 1) {
                if (filtreg(*longitude, atoi(min), atoi(max))) {
                  a = insertionarbreTempPress(a, atof(cl1), atof(cl2));
                }
              }else if (filtre == 2) {
                if (filtrea(*latitude, atoi(min), atoi(max))) {
                  a = insertionarbreTempPress(a, atof(cl1), atof(cl2));
                }
              }else if(filtre == 3){
                if (filtred(date, min, max)) {
                  a = insertionarbreTempPress(a, atof(cl1), atof(cl2));
                }
              }
              else{

                a = insertionarbreTempPress(a, atof(cl1), atof(cl2));

              }
            }
            if(!strcmp(type,"tmp2")||!strcmp(type,"press2")){// si le type utiliser correspond a tmpp (température-pressuin) et que le mode est 2
              printf("FONCTION NON DISPONIBLE\n" );
            }
            if(!strcmp(type,"tmp3")||!strcmp(type,"press3")){// si le type utiliser correspond a tmpp (température-pressuin) et que le mode est 3
              if (filtre == 1) {
                if (filtreg(*longitude, atoi(min), atoi(max))) {
                  a = insertionarbreDate(a, atoi(cl1),cl2,atof(cl3));
                }
              }else if (filtre == 2) {
                if (filtrea(*latitude, atoi(min), atoi(max))) {
                  a = insertionarbreDate(a, atoi(cl1),cl2,atof(cl3));
                }
              }else if(filtre == 3){
                if (filtred(date, min, max)) {
                  a = insertionarbreDate(a, atoi(cl1),cl2,atof(cl3));
                }
              }else{
                a = insertionarbreDate(a, atoi(cl1),cl2,atof(cl3));
              }
            }

            if(!strcmp(type,"vent")){// si le type utiliser correspond a tmpp (température-pressuin) et que le mode est 3

                if (filtre == 1) {
                  if (filtreg(*longitude, atoi(min), atoi(max))) {
                    a = insertionarbreVent(a, atoi(cl1), atof(cl2), atof(cl3));
                  }
                }else if (filtre == 2) {
                  if (filtrea(*latitude, atoi(min), atoi(max))) {
                    a = insertionarbreVent(a, atoi(cl1), atof(cl2), atof(cl3));
                  }
                }else if(filtre == 3){
                  if (filtred(date, min, max)) {
                    a = insertionarbreVent(a, atoi(cl1), atof(cl2), atof(cl3));
                  }
                }else{
                  a = insertionarbreVent(a, atoi(cl1), atof(cl2), atof(cl3));
                }

            }

            if(!strcmp(type,"hauteur")){// si le type utiliser correspond a tmpp (température-pressuin) et que le mode est 3

                if (filtre == 1) {
                  if (filtreg(*longitude, atoi(min), atoi(max))) {
                    a = insertionarbreHauteur(a, atoi(cl1), atoi(cl2));
                  }
                }else if (filtre == 2) {
                  if (filtrea(*latitude, atoi(min), atoi(max))) {
                    a = insertionarbreHauteur(a, atoi(cl1), atoi(cl2));
                  }
                }else if(filtre == 3){
                  if (filtred(date, min, max)) {
                    a = insertionarbreHauteur(a, atoi(cl1), atoi(cl2));
                  }
                }else{
                  a = insertionarbreHauteur(a, atoi(cl1), atoi(cl2));
                }
            }

            if(!strcmp(type,"humidité")){// si le type utiliser correspond a tmpp (température-pressuin) et que le mode est 3

              if (filtre == 1) {
                if (filtreg(*longitude, atoi(min), atoi(max))) {
                  a = insertionarbreHumidite(a, atoi(cl1), atoi(cl2));
                }
              }else if (filtre == 2) {
                if (filtrea(*latitude, atoi(min), atoi(max))) {
                  a = insertionarbreHumidite(a, atoi(cl1), atoi(cl2));
                }
              }else if(filtre == 3){
                if (filtred(date, min, max)) {
                  a = insertionarbreHumidite(a, atoi(cl1), atoi(cl2));
                }
              }else{
                a = insertionarbreHumidite(a, atoi(cl1), atoi(cl2));
              }

            }
            colonne = 0;
            //probleme de dates
            free(tofree);

        }
    }
    return a;
}

/* auteur : Tout le monde
  but : cree un nouveau noeud en fonction des besoins */
  arbre creertemp(int nstation,float val){
  arbre a;
  a=malloc(1*sizeof(noeud));
  a->val=NULL;
  a->val=malloc(sizeof(donnee));
  a->val->nstation=nstation;
  a->val->temp=malloc(sizeof(temp));
  a->val->temp->moyenne=val;
  a->val->temp->max=val;
  a->val->temp->min=val;
  a->val->temp->nbval=1;
  a->fils_g=NULL;
  a->fils_d=NULL;
  return a;
}

  arbre creervent(int nstation,float vitesse, int degre){
  arbre a;
  a=malloc(1*sizeof(noeud));
  a->val=NULL;
  a->val=malloc(sizeof(donnee));
  a->val->nstation=nstation;
  a->val->vent=malloc(sizeof(vent));
  a->val->vent->moyennevitesse=vitesse;
  a->val->vent->moyennedegre=degre;
  a->val->vent->nbval=1;
  a->fils_g=NULL;
  a->fils_d=NULL;
  return a;
}

  arbre creerHauteur(int nstation,int hauteur){
  arbre a;
  a=malloc(1*sizeof(noeud));
  a->val=NULL;
  a->val=malloc(sizeof(donnee));
  a->val->nstation=nstation;
  a->val->hauteur=hauteur;
  a->fils_g=NULL;
  a->fils_d=NULL;
  return a;
}

  arbre creerHumidite(int nstation, int humidite){
  arbre a;
  a=malloc(1*sizeof(noeud));
  a->val=NULL;
  a->val=malloc(sizeof(donnee));
  a->val->nstation=nstation;
  a->val->humidite=humidite;
  a->fils_g=NULL;
  a->fils_d=NULL;
  return a;
}

  arbre creerDate(int nstation , char *date , int temp){
	arbre a;

	a=malloc(1*sizeof(noeud));
	a->val=NULL;
	a->val=malloc(sizeof(donnee));
	a->val->date=malloc(sizeof(arbreDate));
	a->val->nstation=nstation;
	a->val->date->date=date;

	a->val->date->temp=temp;
	a->fils_g=NULL;
	a->fils_d=NULL;
	return a;
}

/* auteur : Gwenn&Nolhan
   but : Ajoute au bon endroit les nouveaux noeuds dans l'ordre des stations et calcule la moyenne de la température*/
  arbre insertionarbreTempPress(arbre a, int nstation, float val){
  if (a == NULL) {
    a = creertemp(nstation,val);
  }else{
    if (nstation > a->val->nstation) {
      a->fils_g = insertionarbreTempPress(a->fils_g, nstation,val);
    }else if (nstation==a->val->nstation){
      if (a->val->temp->max<val){
        a->val->temp->max=val;
      }else if (a->val->temp->min>val){
        a->val->temp->min=val;
      }
      a->val->temp->moyenne=(a->val->temp->moyenne*a->val->temp->nbval+val)/(a->val->temp->nbval+1);
      a->val->temp->nbval=a->val->temp->nbval+1;
      return a;
    }else{
      a->fils_d = insertionarbreTempPress(a->fils_d, nstation,val);
    }
  }
  return a;
}

/* auteur : Nolhan
   but : Ajoute les nouveaux noeuds en fonctions de la date et en cas d'égalité par station */
  arbre insertionarbreDate(arbre a, int nstation,char* date, int temp) {


    if (a == NULL) {
        return creerDate(nstation, date, temp);
    }
    if (iso8601_to_unix(date) < iso8601_to_unix(a->val->date->date)) {
        a->fils_g = insertionarbreDate(a->fils_g, nstation, date, temp);
    } else if (iso8601_to_unix(date) == iso8601_to_unix(a->val->date->date)) {
        if (nstation < a->val->nstation) {
            a->fils_g = insertionarbreDate(a->fils_g, nstation, date, temp);
        } else if(nstation >= a->val->nstation) {
            a->fils_d = insertionarbreDate(a->fils_d, nstation, date, temp);
        }
    } else {
        a->fils_d = insertionarbreDate(a->fils_d, nstation, date, temp);
    }

    return a;
}

/* auteur : Gwenn
   but : Ajoute les nouveaux noeuds en fonctions de la station et calcule la moyenne de la vitesse du vent*/
  arbre insertionarbreVent(arbre a, int nstation, int degre, float vitesse){
  if (a == NULL) {
    a = creervent(nstation,degre,vitesse);
  }else{
    if (nstation < a->val->nstation) {
      a->fils_g = insertionarbreVent(a->fils_g, nstation,degre,vitesse);
    }else if (nstation==a->val->nstation){
      a->val->vent->moyennevitesse=(a->val->vent->moyennevitesse*a->val->vent->nbval+vitesse)/(a->val->vent->nbval+1);
      a->val->vent->moyennedegre=(a->val->vent->moyennedegre*a->val->vent->nbval+degre)/(a->val->vent->nbval+1);
      a->val->vent->nbval=a->val->vent->nbval+1;
      return a;
    }else{
      a->fils_d = insertionarbreVent(a->fils_d, nstation,degre,vitesse);
    }
  }
  return a;
}

/* auteur : Gwenn
   but : Ajoute les nouveaux noeuds en fonctions de la hauteur*/
  arbre insertionarbreHauteur(arbre a, int nstation, int hauteur){
  if (a == NULL) {
    a = creerHauteur(nstation,hauteur);
  }else{
    if (hauteur >= a->val->hauteur) {
      a->fils_g = insertionarbreHauteur(a->fils_g, nstation,hauteur);
    }else{
      a->fils_d = insertionarbreHauteur(a->fils_d, nstation,hauteur);
    }
  }
  return a;
}

/* auteur : Gwenn
   but : Calcule le max d'humidité pour chaque station*/
  arbre maxhumiditestation(arbre a, int nstation, int humidite){
  if (a == NULL) {
    a = creerHumidite(nstation,humidite);
  }
  if (nstation > a->val->nstation) {
    a->fils_g = maxhumiditestation(a->fils_g, nstation,humidite);
  }else if (nstation==a->val->nstation){
    if (a->val->humidite<humidite){
      a->val->humidite=humidite;
    }
  }else{
    a->fils_d = maxhumiditestation(a->fils_d, nstation,humidite);
  }
  return a;
}

/* auteur : Gwenn&Nolhan
   but : Ajoute les nouveaux noeuds en fonction de l'humidité*/
  arbre insererHumidite(arbre b, int nstation, int humidite) {
    if (b == NULL) {
        return creerHumidite(nstation, humidite);
    }
    if (humidite > b->val->humidite) {
        b->fils_g = insererHumidite(b->fils_g, nstation, humidite);
    } else {
        b->fils_d = insererHumidite(b->fils_d, nstation, humidite);
    }
    return b;
}
  arbre insertionarbreHumiditerec(arbre a, arbre b) {
    if (a == NULL) {
        return b;
    }
    b = insertionarbreHumiditerec(a->fils_g, b);
    b = insererHumidite(b, a->val->nstation, a->val->humidite);
    b = insertionarbreHumiditerec(a->fils_d, b);
    return b;
}
  arbre insertionarbreHumidite(arbre a,int nstation,int humidite){
  arbre b;
  b=NULL;
  a=maxhumiditestation(a,nstation,humidite);
  return insertionarbreHumiditerec(a,b);
}

/* auteur : Tout le monde
   but : Ecrire dans un nouveau fichier les données triés en fonction de la demande */
   void sauvegarderArbreRec2(arbre a, FILE* fichier, char* type) {
    if(a != NULL) {
			if (!strcmp(type, "tmp1")||!strcmp(type, "press1")) {
				sauvegarderArbreRec2(a->fils_g, fichier, type);
				fprintf(fichier,"%d;%f;%f;%f\n", a->val->nstation, a->val->temp->min, a->val->temp->max, a->val->temp->moyenne);
				sauvegarderArbreRec2(a->fils_d, fichier, type);
			}

      if (!strcmp(type, "tmp3")||!strcmp(type, "press3")) {
        sauvegarderArbreRec2(a->fils_g, fichier, type);
        fprintf(fichier,"%d;%s;%d\n", a->val->nstation,a->val->date->date,a->val->date->temp);
        sauvegarderArbreRec2(a->fils_d, fichier, type);
      }


			if (!strcmp(type, "vent")) {
				sauvegarderArbreRec2(a->fils_g, fichier, type);
				fprintf(fichier,"%d;%f;%f\n", a->val->nstation,a->val->vent->moyennevitesse,a->val->vent->moyennedegre);
				sauvegarderArbreRec2(a->fils_d, fichier, type);
			}

			if (!strcmp(type, "hauteur")) {
				sauvegarderArbreRec2(a->fils_g, fichier, type);
				fprintf(fichier,"%d;%d\n", a->val->nstation,a->val->hauteur);
				sauvegarderArbreRec2(a->fils_d, fichier, type);
			}
			if (!strcmp(type, "humidité")) {
				sauvegarderArbreRec2(a->fils_g, fichier, type);

				fprintf(fichier,"%d;%d\n", a->val->nstation,a->val->hauteur);
				sauvegarderArbreRec2(a->fils_d, fichier, type);
			}
    }
}

  void sauvegarderArbreRec(arbre a, FILE* fichier, char* type){
	sauvegarderArbreRec2(a, fichier, type);
  }

//FIN
